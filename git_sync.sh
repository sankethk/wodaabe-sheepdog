#!/bin/sh

REPO='/home/pi/Documents/sheepdog'
COMMIT_TIMESTAMP=`date +'%Y-%m-%d %H:%M:%S %Z'`

GIT=`which git`


cd ${REPO}
${GIT} add --all . 
${GIT} commit -m "Automated commit on ${COMMIT_TIMESTAMP}" 
